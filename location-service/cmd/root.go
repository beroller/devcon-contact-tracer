package cmd

import (
	"os"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Default configuration can be set here.
var defaults = map[string]interface{}{
	"kafka.brokers":   []string{"dctx-kafka:9092"},
	"kafka.topic":     "dctx-commands",
	"kafka.partition": 0,
	"kafka.minBytes":  10e3,
	"kafka.maxBytes":  10e6,
}

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "location-service",
	Short: "location-service handles location requests",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(version string) {
	rootCmd.Version = version
	if err := rootCmd.Execute(); err != nil {
		log.Error("Cannot execute root command: ", err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// load the defaults from the default map here, add new defaults on the map
	for key, val := range defaults {
		viper.SetDefault(key, val)
	}

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.location-service.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			log.Error("Could not get home directory: ", err)
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".location-service")
	}

	viper.SetEnvPrefix("API_SERVICE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())
	}
}
