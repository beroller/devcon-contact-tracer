//+build wireinject

package cmd

import (
	"github.com/google/wire"
	"github.com/spf13/viper"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/location-service/internal/server"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/location-service/internal/service"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport"
)

func createServer() *server.LocationServer {
	wire.Build(
		ProvideKafka,
		service.NewCheckInService,
		server.NewLocationServer,
	)
	return &server.LocationServer{}
}

func ProvideKafka() transport.MessageBus {
	kafkaConfig := transport.NewKafkaConfig(
		viper.GetStringSlice("kafka.brokers"),
		viper.GetInt("kafka.minBytes"),
		viper.GetInt("kafka.maxBytes"))
	return transport.NewKafka(kafkaConfig)
}
