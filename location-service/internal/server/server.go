package server

import (
	"time"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/location-service/internal/service"
)

type LocationServer struct {
	*service.CheckInService
}

func NewLocationServer(s *service.CheckInService) *LocationServer {
	return &LocationServer{s}
}

// Run runs the server and it's services
func (a *LocationServer) Run() error {
	a.CheckInService.Start()
	for {
		time.Sleep(1 * time.Second)
	}
}
