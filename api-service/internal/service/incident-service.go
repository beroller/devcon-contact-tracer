package service

import (
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport"
)

type IncidentService struct {
	transport.MessageBus
}

func NewIncidentService(mb transport.MessageBus) *IncidentService {
	return &IncidentService{mb}
}

func (s *IncidentService) Report(incident *model.Incident) error {
	return nil
}
