package server

import (
	"fmt"
	"net/http"

	"github.com/gorilla/handlers"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/handler"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
)

type APIServer struct {
	*mux.Router
	*Config
	handlers []handler.Routable
	// filters  []mux.MiddlewareFunc
}

type Config struct {
	Host           string
	Port           int
	Spec           string
	AllowedHeaders []string
	AllowedOrigins []string
	AllowedMethods []string
}

func NewAPIServerConfig(host string, port int, spec string, allowedOrigins, allowedHeaders, allowedMethods []string) *Config {
	return &Config{
		Host:           host,
		Port:           port,
		Spec:           spec,
		AllowedOrigins: allowedOrigins,
		AllowedHeaders: allowedHeaders,
		AllowedMethods: allowedMethods,
	}
}

func NewAPIServer(apiserverConfig *Config, router *mux.Router, handlers []handler.Routable) *APIServer {
	return &APIServer{
		Config:   apiserverConfig,
		Router:   router,
		handlers: handlers,
	}
}

func (s *Config) String() string {
	return fmt.Sprintf("%s:%d", s.Host, s.Port)
}

// Run runs the server and it's services
func (a *APIServer) Run() error {
	a.serveOpenAPI()
	a.registerRoutes()
	a.registerFilter()
	address := a.Config.String()
	log.WithField("address", address).Info("API Server started")
	return http.ListenAndServe(address, a.Router)
}

func (a *APIServer) serveOpenAPI() {
	a.Router.HandleFunc("/spec/openapi.yaml", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, a.Spec)
	})
	log.Info("/spec/openapi.yaml registered")
}

func (a *APIServer) registerRoutes() {
	for _, routable := range a.handlers {
		routable.Register(a.Router)
	}
}

func (a *APIServer) registerFilter() {
	corsFilter := handlers.CORS(
		handlers.AllowedOrigins(a.AllowedOrigins),
		handlers.AllowedHeaders(a.AllowedHeaders),
		handlers.AllowedMethods(a.AllowedMethods),
	)
	a.Use(corsFilter)
}
