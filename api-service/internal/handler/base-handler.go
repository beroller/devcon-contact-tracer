package handler

import (
	"github.com/gorilla/mux"
)

type Routable interface {
	Register(*mux.Router)
}
