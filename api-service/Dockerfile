##
## We build everything here
##
FROM golang:alpine as build

##
## Add git, ca-certificates and timezone info, needed if we call external services
##
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

##
## Add a new user here since we can't add it in scratch
##
ENV USER=dctx \
    UID=10001
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"

##
## Build the go binary here. CGO_ENABLED=0 to disable clib requirement for image to work in scratch
##
ENV CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
ARG VERSION=dev
WORKDIR /go/src/gitlab.com/dctx/devcon-contact-tracker/api-service
COPY go.* ./
RUN go mod download
COPY . .
RUN go build -o /go/bin/api-service -ldflags "-X main.version=${VERSION} -w -s"


##
## Final image uses scratch. We copy zoneinfo, ca-certs, user/group details, and the binary from the previous step
##
FROM scratch
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
COPY --from=build /go/bin/api-service /api-service
COPY dctx-openapi.yaml /dctx-openapi.yaml

##
## This image contains the migration files
##
# ADD db /db

##
## Set to the unprivileged user
##
USER dctx:dctx

##
## Set the binary as the entrypoint
##
ENTRYPOINT [ "/api-service" ]
CMD [ "serve" ]