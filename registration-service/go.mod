module gitlab.com/dctx/devcon-contact-tracer/registration-service

go 1.14

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
)
