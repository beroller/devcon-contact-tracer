//+build wireinject

package cmd

import (
	"github.com/google/wire"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/domain/checkin"
	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/driver"
	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/handler"
	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/server"
)

func createServer() (*server.APIServer, error) {
	wire.Build(
		driver.CreateDriver,
		checkin.NewService,
		ProvideServerConfig,
		mux.NewRouter,
		server.NewAPIServer,
		handler.NewCheckinHandler,
	)
	return &server.APIServer{}, nil
}

func ProvideServerConfig() *server.Config {
	return server.NewAPIServerConfig(
		viper.GetString("server.host"),
		viper.GetInt("server.port"))
}
