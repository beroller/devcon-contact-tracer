package checkin

import (
	"time"
)

type Location struct {
	Lat  string
	Long string
}

type CheckIn struct {
	ID           int64 `json:"id"`
	Location     `json:"location"`
	Timestamp    time.Time `json:"timestamp"`
	MobileNumber string    `json:"mobileNumber"`
}
