package checkin

import (
	"github.com/mitchellh/mapstructure"
	"github.com/neo4j/neo4j-go-driver/neo4j"
)

type checkinService struct {
	driver neo4j.Driver
}

type CheckinService interface {
	CheckIn(data CheckIn) (CheckIn, error)
	ListCheckIn() ([]CheckIn, error)
}

func NewService(driver neo4j.Driver) CheckinService {
	return &checkinService{
		driver: driver,
	}
}

func (checkinService *checkinService) CheckIn(data CheckIn) (CheckIn, error) {
	// Persist a CheckIn record to Neo4j

	// TODO implementation for CheckIn

	// Create a session (a connection to the graphdb) the driver maintains a pool of connections

	// session, err = checkinStore.driver.Session(neo4j.AccessModeWrite)
	// if err != nil {
	// 	return CheckIn{}, err
	// }
	// defer session.Close()

	// result, err := session.WriteTransaction("CREATE(c: CheckIn {mobileNumber: $mobileNumber} )",
	// 	map[string]interface{}{
	// 		"mobileNumber": person.MobileNumber,
	// 	}
	// )
	// if err != nil {
	// 	return CheckIn{}, err
	// }

	return CheckIn{}, nil
}

func (checkinService *checkinService) ListCheckIn() ([]CheckIn, error) {
	var (
		checkinList []CheckIn
		checkin     CheckIn
	)
	session, err := checkinService.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return []CheckIn{}, err
	}
	defer session.Close()

	result, err := session.ReadTransaction(func(transaction neo4j.Transaction) (interface{}, error) {
		result, err := transaction.Run("MATCH(c: Checkin) RETURN c", map[string]interface{}{})
		if err != nil {
			return nil, err
		}

		if result.Next() {
			return result, err
		}

		return nil, result.Err()
	})

	// Convert neo4j Result to our struct
	for result.(neo4j.Result).Next() {
		returnedProps := result.(neo4j.Node).Props()
		cfg := &mapstructure.DecoderConfig{
			Metadata: nil,
			Result:   &checkin,
			TagName:  "json",
		}
		decoder, _ := mapstructure.NewDecoder(cfg)
		decoder.Decode(returnedProps)

		checkinList = append(checkinList, checkin)
	}

	return checkinList, result.(neo4j.Result).Err()
}
