##
## We build everything here, ubuntu because of seabolt, have to build everything from scratch
##
FROM ubuntu:bionic as build

##
## Add a new user here since, we will copy this to the final image
##
ENV USER=dctx \
    UID=10001
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"

##
## Install go and seabolt
##
ENV GO_VERSION=1.14.1 \
    SEABOLT_VERSION=1.7.4 \
    PATH=${PATH}:/usr/local/go/bin
RUN apt-get update && \
    apt-get install -y libssl1.1 wget git pkg-config libssl-dev && \
    wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz && \
    mkdir -p /go/{bin,pkg,src} && \
    wget https://github.com/neo4j-drivers/seabolt/releases/download/v${SEABOLT_VERSION}/seabolt-${SEABOLT_VERSION}-Linux-ubuntu-18.04.deb && \
    dpkg -i seabolt-${SEABOLT_VERSION}-Linux-ubuntu-18.04.deb

##
## Build the go binary here. CGO_ENABLED=0 to disable clib requirement for image to work in scratch
##
ENV GOPATH=/go \
    GOSUMDB=off \
    GO111MODULE=on
ARG VERSION=dev
WORKDIR /go/src/gitlab.com/dctx/devcon-contact-tracker/graph-service
COPY go.* ./
RUN go mod download
COPY . .
RUN go build -o /go/bin/graph-service -ldflags "-X main.version=${VERSION} -w -s" --tags seabolt_static

##
## Final image uses scratch. We copy zoneinfo, ca-certs, user/group details, and the binary from the previous step
##
FROM ubuntu:bionic
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
COPY --from=build /go/bin/graph-service /graph-service

##
## This image contains the migration files
##
ADD db /db

##
## Set to the unprivileged user
##
USER dctx:dctx

##
## Set the binary as the entrypoint
##
ENTRYPOINT [ "/graph-service" ]
CMD [ "serve" ]