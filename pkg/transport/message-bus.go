package transport

import (
	"io"
)

type Message struct {
	Key   []byte
	Value []byte
}

type OnMessageReceived func(*Message)

type MessageBus interface {
	Publish(topic string, msgs ...Message) error
	Subscribe(groupID, topic string, callback OnMessageReceived) (io.Closer, error)
}
